// Oppgave2
// fetchUser.js
// -- Henter user og userdata --
// https://developer.mozilla.org/en-US/docs/Web/API/Event/composedPath


function fetchUsers() {
  fetch ('api/fetchUsers.php')
  .then(res=>res.json())
  .then(users=>{
    const userUL = document.querySelector('.users ul');
    userUL.innerHTML = '';
    // Henter brukerinformasjon.
    users.forEach(user=>{
      const userLI = document.createElement ('LI');
      userLI.setAttribute ('data-uid', user.uid);
      userLI.innerHTML += `<span>${user.firstName}
                                 ${user.lastName}</span>
                           <span>${user.uname}</span>`;
      userUL.appendChild (userLI);
    })
    userUL.addEventListener('click', e=>{

      //Muligheten til å klikke i Fetch-listen
      //For å velge en user å redigere.
      // Dette setter også ID'en så det er nødvendig for å gjøre endringer.
      if (e.composedPath()[1].tagName=='LI') {
        editUser (e.composedPath()[1].dataset['uid']);
      } else if (e.target.tagName=='LI') {
        editUser (e.target.dataset['uid']);
      }
    })
  })
}

fetchUsers();
