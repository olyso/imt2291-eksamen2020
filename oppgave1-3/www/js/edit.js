// Oppgave 2


window.uid = -1;

// Inspired from the practice tasks.
// Also https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
// to a larger degree.
function editUser(uid) {
  fetch(`api/fetchUser.php?id=${uid}`)
  .then(res=>res.json())
  .then(user=>{
    //Json declarations.
    window.uid = user.uid;
    document.getElementById('uname').value = user.uname;
    document.getElementById('firstName').value = user.firstName;
    document.getElementById('lastName').value = user.lastName;
  })
}

// ---- ONCLICK --
document.querySelector('input[type="submit"]').addEventListener('click', e=>{
  console.log("Clickcheck");
  e.preventDefault();


  if (window.uid>-1) {
    // Start formdata som skal sendes til
    //update.
    const data = new FormData(e.target.form);
    data.append('uid', window.uid);

    console.log("LINE 21 WORKS");

    fetch('api/updateUser.php', {
      method: 'POST',
      body: data // Post to the API.
    }).then(res=>res.json())
    .then(data=>{
      const response = document.querySelector('.response');
      response.style.display = 'block';


      // ---- Feedback ----
      if (data.status=='success') {
        response.style.color = "#2C3";
        response.innerHTML = "Oppdaterte brukeren!!!";
        fetchUsers();
        }
      else {
        response.style.color = "#C24";
        response.innerHTML = "Feilet å gjøre endringer!";
        console.log(data.msg);
      }
      setTimeout(()=> {
        response.style.display = 'none';
      }, 3000);
    })


  }
  console.log("Successful run.");

})
