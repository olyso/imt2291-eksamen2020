<?php
// ------------- OPPGAVE 1 ---------
//----------------------------------
require_once ('../classes/DB.php');
require_once '../vendor/autoload.php';
$loader = new Twig_Loader_Filesystem('../templates');
$twig = new Twig_Environment($loader, array(
    /*'cache' => './compilation_cache',*/ /* Only enable cache when everything works correctly */
));
$db = DB::getDBConnection();
if (!isset($_POST['email'])) {  // Show form to create user
  echo $twig->render('registerUser.html', array());
} else {

if(isset($_POST['email']) && isset($_POST['password']) && isset($_POST['firstname']) && isset($_POST['lastname'])){
  $error['failed'] = 0;
  if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {$error['failed'] = 1; $error['message'] += " Ugyldig epost!" ;}
  else if(strlen($_POST['password']) < 4) { $error['failed'] = 1; $error['message'] += " For kort passord!"; }
  else if(strlen($_POST['firstname']) < 3) { $error['failed'] = 1; $error['message'] += " For kort fornavn!"; }
  else if(strlen($_POST['lastname']) < 3) { $error['failed'] = 1;$error['message'] += " For kort etternavn!"; }
  // jeg bruker += på errorMessage i tilfelle man har flere errors.

}
if($error['failed'] == 1){
echo $twig->render('registerUser.html',$error);
}else{
  $query = $db->prepare("INSERT into user (uname, pwd,firstName,lastName) VALUES (:email,:password,:firstname,:lastname)");
  $query -> bindParam(':email',$_POST['email']);
  $query -> bindParam(':password',$_POST['password']);
  $query -> bindParam(':firstname',$_POST['firstname']);
  $query -> bindParam(':lastname',$_POST['lastname']);
  $query->execute();
  if($query->rowCount() == 1){
    echo "User added!";
  }else{echo $twig->render('registerUser.html',$error);}
}


}




 ?>
